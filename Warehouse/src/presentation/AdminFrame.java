package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class AdminFrame extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	public static void triggerWindow()
	{
		JFrame adminFrame = new JFrame("Admin");
		AdminFrame content = new AdminFrame();
		adminFrame.setContentPane(content);
		adminFrame.setLocation(150, 300);
		adminFrame.setSize(new Dimension(1000,150));
		adminFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		adminFrame.setVisible(true);
	}
	
	JButton addClient;
	JButton deleteClient;
	JButton viewClients;
	JButton addProduct;
	JButton updateProduct;
	JButton deleteProduct;
	JButton viewProducts;
	
	public AdminFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.cyan);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(1,7));
		centerPanel.setBackground(Color.cyan);
		this.add(centerPanel, BorderLayout.CENTER);
		
		addClient = new JButton("Add Client");
		addClient.addActionListener(this);
		centerPanel.add(addClient);
		
		deleteClient = new JButton("Delete Client");
		deleteClient.addActionListener(this);
		centerPanel.add(deleteClient);
		
		viewClients = new JButton("View Clients");
		viewClients.addActionListener(this);
		centerPanel.add(viewClients);
		
		addProduct = new JButton("Add Product");
		addProduct.addActionListener(this);
		centerPanel.add(addProduct);
		
		deleteProduct = new JButton("Delete Product");
		deleteProduct.addActionListener(this);
		centerPanel.add(deleteProduct);
		
		updateProduct = new JButton("Update Product");
		updateProduct.addActionListener(this);
		centerPanel.add(updateProduct);
		
		viewProducts = new JButton("View Products");
		viewProducts.addActionListener(this);
		centerPanel.add(viewProducts);
		
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Add Client"))
		{
			AddClientFrame.triggerWindow();
		}
		else if(s.equals("Delete Client"))
		{
			DeleteClientFrame.triggerWindow();
		}
		else if(s.equals("View Clients"))
		{
			ViewClientsFrame.triggerWindow();
		}
		else if(s.equals("Add Product"))
		{
			AddProductFrame.triggerWindow();
		}
		else if(s.equals("Delete Product"))
		{
			DeleteProductFrame.triggerWindow();
		}
		else if(s.equals("Update Product"))
		{
			UpdateProductFrame.triggerWindow();
		}
		else if(s.equals("View Products"))
		{
			ViewProductsFrame.triggerWindow();
		}
	}

}
