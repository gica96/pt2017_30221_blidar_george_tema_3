package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import dataAccessObject.CustomerDAO;

public class MainFrame extends JPanel implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;

	public static void triggerFrame()
	{
		JFrame window = new JFrame("Warehouse");
		MainFrame content = new MainFrame();
		window.setContentPane(content);
		window.pack();
		window.setLocation(400,200);
		window.setSize(new Dimension(500,300));
		window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		window.setVisible(true);
		window.setResizable(true);
		copyFrame = window;
	}
	
	JLabel user;
	JLabel pass;
	JTextField userField;
	JTextField passField;
	JButton loginButton;
	
	public MainFrame()
	{
		this.setBackground(Color.GREEN);
		this.setLayout(new BorderLayout());
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridBagLayout());
		centerPanel.setBackground(Color.orange);
		this.add(centerPanel);
		
		user = new JLabel("Name: ");
		centerPanel.add(user);
		
		userField = new JTextField();
		userField.setPreferredSize(new Dimension(150,50));
		centerPanel.add(userField);
		
		pass = new JLabel("Pass: ");
		centerPanel.add(pass);
		
		passField = new JPasswordField();
		passField.setPreferredSize(new Dimension(150,50));
		centerPanel.add(passField);
		
		loginButton = new JButton("Login");
		loginButton.setBounds(0,0,50,50);
		centerPanel.add(loginButton);
		loginButton.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Login"))
		{
			int result[] = new int[2]; 
			result = CustomerDAO.identifyCustomer(userField.getText(), passField.getText());
			try
			{
				if(result[0] == -1)
				{
					if(result[1] == -1)
					{
						throw new IllegalArgumentException("Tabela de customers este goala!");
					}
					else if(result[1] == 0)
					{
						throw new IllegalArgumentException("Acest customer nu exista in baza de date!");
					}
				}
				else if(result[0] == 0)
				{
					if(result[1] == 0)
					{
						throw new IllegalArgumentException("Parola este incorecta!");
					}
				}
				else if(result[0] == 1)
				{
					if(result[1] == 1)
					{
						AdminFrame.triggerWindow();
						copyFrame.dispose();
					}
					else
					{
						int id = CustomerDAO.getCustomerId(userField.getText());
						UserFrame.triggerWindow(id);
					}
				}
			}
			catch(IllegalArgumentException ex)
			{
				JFrame errorFrame = new JFrame("Eroare");
				JPanel errorPanel = new JPanel();
				JLabel errorLabel = new JLabel(ex.getMessage());
				errorPanel.add(errorLabel);
				errorFrame.setContentPane(errorPanel);
				errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				errorFrame.setSize(200,115);
				errorFrame.setLocation(400,400);
				errorFrame.setVisible(true);
				errorFrame.setEnabled(true);
			}
		}
	}

}
