package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataAccessObject.ProductDAO;
import productValidator.ProductValidator;

public class UpdateFrame extends JPanel implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	private static int toUpdateProductId;
	static JFrame copyFrame;
	
	public static void triggerWindow(int productId)
	{
		JFrame updateFrame = new JFrame("Update Product second Frame");
		UpdateFrame content = new UpdateFrame();
		updateFrame.setContentPane(content);
		updateFrame.setLocation(500, 300);
		updateFrame.setSize(new Dimension(500,150));
		updateFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		updateFrame.setVisible(true);
		toUpdateProductId = productId;
		copyFrame = updateFrame;
	}
	
	JLabel productQuantity;
	JTextField productQuantityField;
	JLabel productPrice;
	JTextField productPriceField;
	JButton updateButton;
	
	public UpdateFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.YELLOW);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.YELLOW);
		centerPanel.setLayout(new GridLayout(2,2));
		this.add(centerPanel,BorderLayout.CENTER);
		
		productQuantity = new JLabel("Product's new Quantity");
		centerPanel.add(productQuantity);
		
		productQuantityField = new JTextField();
		productQuantityField.setPreferredSize(new Dimension(50,50));
		centerPanel.add(productQuantityField);
		
		productPrice = new JLabel("Product's new Price");
		centerPanel.add(productPrice);
		
		productPriceField = new JTextField();
		productPriceField.setPreferredSize(new Dimension(50,50));
		centerPanel.add(productPriceField);
		
		updateButton = new JButton("Update Product!");
		updateButton.addActionListener(this);
		this.add(updateButton,BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		try
		{
			if(s.equals("Update Product!"))
			{
				boolean valid;
				ProductValidator validator = new ProductValidator();
				valid = validator.validateQuantity(Integer.parseInt(productQuantityField.getText()));
				if(valid == false)
				{
					throw new SQLException("Cantitate invalida!");
				}
				valid = validator.validatePrice(Double.parseDouble(productPriceField.getText()));
				if(valid == false)
				{
					throw new SQLException("Pret invalid!");
				}
				if(valid)
				{
					ProductDAO.updateProduct(toUpdateProductId, Integer.parseInt(productQuantityField.getText()), Double.parseDouble(productPriceField.getText()));
					copyFrame.dispose();
				}
			}
		}
		catch(SQLException ex)
		{
			JFrame errorFrame = new JFrame("Eroare");
			JPanel errorPanel = new JPanel();
			JLabel errorLabel = new JLabel(ex.getMessage());
			errorPanel.add(errorLabel);
			errorFrame.setContentPane(errorPanel);
			errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			errorFrame.setSize(200,115);
			errorFrame.setLocation(400,400);
			errorFrame.setVisible(true);
			errorFrame.setEnabled(true);
		}
	}
}
