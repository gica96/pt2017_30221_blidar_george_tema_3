package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataAccessObject.CustomerDAO;
import dataAccessObject.OrderDAO;
import dataAccessObject.ProductDAO;
import email.EmailAttachment;
import model.Stock;

public class UserFrame extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static int customerId;
	private FileWriter writer;
	
	public static void triggerWindow(int userId)
	{
		JFrame userFrame = new JFrame("User");
		UserFrame content = new UserFrame();
		userFrame.setContentPane(content);
		userFrame.setLocation(350, 150);
		userFrame.setSize(new Dimension(400,200));
		userFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		userFrame.setVisible(true);
		customerId = userId;
	}
	
	JButton viewProducts;
	JLabel productName;
	JTextField productNameField;
	JLabel quantity;
	JTextField quantityField;
	JButton purchaseButton;
	
	public UserFrame()
	{
		try
		{
			writer = new FileWriter("Chitanta.txt",true);
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		this.setLayout(new BorderLayout());
		this.setBackground(Color.cyan);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(2,2));
		centerPanel.setBackground(Color.cyan);
		this.add(centerPanel, BorderLayout.CENTER);
		
		viewProducts = new JButton("View Products");
		viewProducts.addActionListener(this);
		this.add(viewProducts,BorderLayout.NORTH);
		
		productName = new JLabel("Product's Name");
		centerPanel.add(productName);
		
		productNameField = new JTextField();
		centerPanel.add(productNameField);
		
		quantity = new JLabel("Quantity");
		centerPanel.add(quantity);
		
		quantityField = new JTextField();
		centerPanel.add(quantityField);
		
		purchaseButton = new JButton("Purchase");
		purchaseButton.addActionListener(this);
		this.add(purchaseButton,BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("View Products"))
		{
			ViewProductsFrame.triggerWindow();
		}
		else if(s.equals("Purchase"))
		{
			int productId = ProductDAO.getProductId(productNameField.getText());
			int quantity = 0;
			ArrayList<Stock> stock = ProductDAO.getStock();
			for(Stock theStock : stock)
			{
				if(theStock.getProductId() == productId)
				{
					quantity = theStock.getQuantity();
					break;
				}
			}
			try
			{
				if(Integer.parseInt(quantityField.getText()) > quantity)
				{
					throw new SQLException("Doriti sa cumparati prea multe produse!");
				}
				else
				{
					OrderDAO.insertOrder(customerId, productId);
					int remainingQuantity = quantity - Integer.parseInt(quantityField.getText());
					ProductDAO.updateStock(productId, remainingQuantity);
					Double price = ProductDAO.getPrice(productId);
					int actualQuantity = Integer.parseInt(quantityField.getText());
					double finalPrice = price * actualQuantity;
					try
					{
						writer.write("Clientul cu id-ul: " + customerId + " a comandat produsul: " + productNameField.getText() + " in cantitate de: " + actualQuantity + " bucati." + " Plata finala este in valoare de: " + finalPrice);
						writer.close();
						String email = CustomerDAO.getEmailAdress(customerId);
						String host = "smtp.gmail.com";
				        String port = "587";
				        String mailFrom = "georgeblidar96@gmail.com";
				        String password = "ilikerogerfederer";
				        String mailTo = email;
				        String subject = "Chitanta";
				        String message = "Aici aveti chitanta!";
				        
				        String[] attachFiles = new String[1];
				        attachFiles[0] = "D:\\Proiecte Java\\Warehouse\\Chitanta.txt";
				        try 
				        {
				            EmailAttachment.sendEmailWithAttachments(host, port, mailFrom, password, mailTo,
				                subject, message, attachFiles);
				            System.out.println("Email sent.");
				        } catch (Exception ex) {
				            System.out.println("Could not send email.");
				            ex.printStackTrace();
				        }
					}
					catch(IOException ex)
					{
						ex.printStackTrace();
					}
				}
			}
			catch(SQLException ex)
			{
				JFrame errorFrame = new JFrame("Eroare");
				JPanel errorPanel = new JPanel();
				JLabel errorLabel = new JLabel(ex.getMessage());
				errorPanel.add(errorLabel);
				errorFrame.setContentPane(errorPanel);
				errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				errorFrame.setSize(200,115);
				errorFrame.setLocation(400,400);
				errorFrame.setVisible(true);
				errorFrame.setEnabled(true);
			}
		}
	}
}
