package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dataAccessObject.ProductDAO;
import dataAccessObject.Table;
import model.Product;
import model.Stock;

public class ViewProductsFrame extends JPanel {

	private static final long serialVersionUID = 1L;
	
	public static void triggerWindow()
	{
		JFrame tableProductFrame = new JFrame("Product Info");
		ViewProductsFrame content = new ViewProductsFrame();
		tableProductFrame.setContentPane(content);
		tableProductFrame.setLocation(150, 300);
		tableProductFrame.setSize(new Dimension(1000,150));
		tableProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		tableProductFrame.setVisible(true);
		tableProductFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	public ViewProductsFrame()
	{
		this.setLayout(new BorderLayout());
		ArrayList<Product> products = new ArrayList<Product>();
		ArrayList<Stock> stock = new ArrayList<Stock>();
		products = ProductDAO.getAllProducts();
		stock = ProductDAO.getStock();
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ProductID");
		model.addColumn("Name");
		model.addColumn("Description");
		model.addColumn("Price (lei)");
		JTable table = new JTable(model);
		Vector<Object> row = new Vector<Object>();
		
		DefaultTableModel model2 = new DefaultTableModel();
		model2.addColumn("ProductID");
		model2.addColumn("Quantity");
		JTable table2 = new JTable(model2);
		
		for(Product p : products)
		{
			row = Table.retrieveProperties(p);
			model.addRow(row);
		}
		
		for(Stock s : stock)
		{
			row = Table.retrieveProperties(s);
			model2.addRow(row);
		}
		
		JScrollPane pane = new JScrollPane(table);
		this.add(pane, BorderLayout.CENTER);
		
		JScrollPane pane2 = new JScrollPane(table2);
		this.add(pane2,BorderLayout.NORTH);
	}
}
