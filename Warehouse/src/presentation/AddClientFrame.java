package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.CustomerBL;
import dataAccessObject.CustomerDAO;
import model.Customer;

public class AddClientFrame extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;
	
	public static void triggerWindow()
	{
		JFrame addClientFrame = new JFrame("Add Client");
		AddClientFrame content = new AddClientFrame();
		addClientFrame.setContentPane(content);
		addClientFrame.setLocation(500,100);
		addClientFrame.setSize(new Dimension(500,500));
		addClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addClientFrame.setVisible(true);
		copyFrame = addClientFrame;
	}
	
	JLabel id;
	JLabel name;
	JLabel pass;
	JLabel phone;
	JLabel adress;
	JLabel email;
	JLabel admin;
	JTextField idField;
	JTextField nameField;
	JTextField passField;
	JTextField phoneField;
	JTextField adressField;
	JTextField emailField;
	JTextField adminField;
	JButton addCustomer;
	
	public AddClientFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.cyan);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(7,2));
		this.add(centerPanel,BorderLayout.CENTER);
		
		id = new JLabel("Customer ID");
		centerPanel.add(id);
		
		idField = new JTextField();
		centerPanel.add(idField);
		
		name = new JLabel("Customer's name");
		centerPanel.add(name);
		
		nameField = new JTextField();
		centerPanel.add(nameField);
		
		pass = new JLabel("Customer's password");
		centerPanel.add(pass);
		
		passField = new JTextField();
		centerPanel.add(passField);
		
		phone = new JLabel("Phone number");
		centerPanel.add(phone);
		
		phoneField = new JTextField();
		centerPanel.add(phoneField);
		
		adress = new JLabel("Adress");
		centerPanel.add(adress);
		
		adressField = new JTextField();
		centerPanel.add(adressField);
		
		email = new JLabel("Email");
		centerPanel.add(email);
		
		emailField = new JTextField();
		centerPanel.add(emailField);
		
		admin = new JLabel("Admin");
		centerPanel.add(admin);
		
		adminField = new JTextField();
		centerPanel.add(adminField);
		
		addCustomer = new JButton("Add Customer");
		addCustomer.addActionListener(this);
		this.add(addCustomer,BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Add Customer"))
		{
			Customer customer;
			CustomerBL customerBL = new CustomerBL();
			boolean valid;
			customer = new Customer(Integer.parseInt(idField.getText()),nameField.getText(),passField.getText(),phoneField.getText(),adressField.getText(),emailField.getText(),Integer.parseInt(adminField.getText()));
			valid = customerBL.validateCustomer(customer);
			if(valid)
			{
				try
				{
					int insertedSuccesfully = CustomerDAO.insertCustomer(customer);
					if(insertedSuccesfully == 0)
					{
						throw new SQLException("Inserarea nu a avut loc cu succes...");
					}
				}
				catch(SQLException ex)
				{
					JFrame errorFrame = new JFrame("Eroare");
					JPanel errorPanel = new JPanel();
					JLabel errorLabel = new JLabel(ex.getMessage());
					errorPanel.add(errorLabel);
					errorFrame.setContentPane(errorPanel);
					errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					errorFrame.setSize(200,115);
					errorFrame.setLocation(400,400);
					errorFrame.setVisible(true);
					errorFrame.setEnabled(true);
				}
			}
			copyFrame.dispose();
		}
	}

}
