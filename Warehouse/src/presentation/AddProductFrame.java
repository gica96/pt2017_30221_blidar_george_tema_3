package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ProductBL;
import dataAccessObject.ProductDAO;
import model.Product;


public class AddProductFrame extends JPanel implements ActionListener
{

	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;

	public static void triggerWindow()
	{
		JFrame addProductFrame = new JFrame("Add Product");
		AddProductFrame content = new AddProductFrame();
		addProductFrame.setContentPane(content);
		addProductFrame.setLocation(500,200);
		addProductFrame.setSize(new Dimension(500,250));
		addProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addProductFrame.setVisible(true);
		copyFrame = addProductFrame;
	}
	
	JLabel id;
	JTextField idField;
	JLabel name;
	JTextField nameField;
	JLabel description;
	JTextField descriptionField;
	JLabel price;
	JTextField priceField;
	JButton addProduct;
	
	public AddProductFrame()
	{
		this.setLayout(new BorderLayout());
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(4,2));
		this.add(centerPanel, BorderLayout.CENTER);
		
		id = new JLabel("Product ID");
		centerPanel.add(id);
		
		idField = new JTextField();
		centerPanel.add(idField);
		
		name = new JLabel("Product Name");
		centerPanel.add(name);
		
		nameField = new JTextField();
		centerPanel.add(nameField);
		
		description = new JLabel("Product Description");
		centerPanel.add(description);
		
		descriptionField = new JTextField();
		centerPanel.add(descriptionField);
		
		price = new JLabel("Product's Price");
		centerPanel.add(price);
		
		priceField = new JTextField();
		centerPanel.add(priceField);
		
		addProduct = new JButton("Add Product");
		addProduct.addActionListener(this);
		this.add(addProduct,BorderLayout.SOUTH);
		
		}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Add Product"))
		{
			Product p;
			ProductBL productBL = new ProductBL();
			boolean valid;				
				
			p = new Product(Integer.parseInt(idField.getText()),nameField.getText(), descriptionField.getText(),Double.parseDouble(priceField.getText()));
			valid = productBL.validateProduct(p);
			if(valid)
			{
				try
				{
					int insertedSuccesfully = ProductDAO.insertProduct(p);
					if(insertedSuccesfully == 0)
					{
						throw new SQLException("Inserarea nu a avut loc cu succes...");
					}
				}
				catch(SQLException ex)
				{
					JFrame errorFrame = new JFrame("Eroare");
					JPanel errorPanel = new JPanel();
					JLabel errorLabel = new JLabel(ex.getMessage());
					errorPanel.add(errorLabel);
					errorFrame.setContentPane(errorPanel);
					errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					errorFrame.setSize(200,115);
					errorFrame.setLocation(400,400);
					errorFrame.setVisible(true);
					errorFrame.setEnabled(true);
				}
			}
			copyFrame.dispose();
		}
	}
}
