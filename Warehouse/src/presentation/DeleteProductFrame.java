package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataAccessObject.ProductDAO;

public class DeleteProductFrame extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;
	public static void triggerWindow()
	{
		JFrame deleteProductFrame = new JFrame("Delete Product");
		DeleteProductFrame content = new DeleteProductFrame();
		deleteProductFrame.setContentPane(content);
		deleteProductFrame.setLocation(500, 300);
		deleteProductFrame.setSize(new Dimension(500,150));
		deleteProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		deleteProductFrame.setVisible(true);
		copyFrame = deleteProductFrame;
	}
	
	JLabel productName;
	JTextField productNameField;
	JButton deleteButton;
	
	public DeleteProductFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.RED);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.RED);
		centerPanel.setLayout(new GridLayout(1,2));
		this.add(centerPanel,BorderLayout.CENTER);
		
		productName = new JLabel("Product's Name");
		centerPanel.add(productName);
		
		productNameField = new JTextField();
		productNameField.setPreferredSize(new Dimension(50,50));
		centerPanel.add(productNameField);
		
		deleteButton = new JButton("Delete Product by Name");
		deleteButton.addActionListener(this);
		this.add(deleteButton,BorderLayout.SOUTH);
	}
	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Delete Product by Name"))
		{
			int id = ProductDAO.getProductId(productNameField.getText());
			ProductDAO.deleteProduct(id);
		}
		copyFrame.dispose();
	}

}
