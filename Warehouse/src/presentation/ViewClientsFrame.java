package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dataAccessObject.CustomerDAO;
import dataAccessObject.Table;
import model.Customer;

public class ViewClientsFrame extends JPanel 
{

	private static final long serialVersionUID = 1L;

	public static void triggerWindow()
	{
		JFrame tableClientFrame = new JFrame("Client Info");
		ViewClientsFrame content = new ViewClientsFrame();
		tableClientFrame.setContentPane(content);
		tableClientFrame.setLocation(150, 300);
		tableClientFrame.setSize(new Dimension(1000,150));
		tableClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		tableClientFrame.setVisible(true);
	}
	
	public ViewClientsFrame()
	{
		this.setLayout(new BorderLayout());
		ArrayList<Customer> customers = new ArrayList<Customer>();
		customers = CustomerDAO.getAllCustomers();
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("CustomerID");
		model.addColumn("Name");
		model.addColumn("Password");
		model.addColumn("Phone Number");
		model.addColumn("Adress");
		model.addColumn("Email");
		model.addColumn("Admin");
		JTable table = new JTable(model);
		Vector<Object> row = new Vector<Object>();
		
		for(Customer c : customers)
		{
			row = Table.retrieveProperties(c);
			model.addRow(row);
		}
		JScrollPane pane = new JScrollPane(table);
		this.add(pane, BorderLayout.CENTER);
	}
}
