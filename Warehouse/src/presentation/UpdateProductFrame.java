package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataAccessObject.ProductDAO;

public class UpdateProductFrame extends JPanel implements ActionListener
{
	
	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;
	
	public static void triggerWindow()
	{
		JFrame updateProductFrame = new JFrame("Update Product");
		UpdateProductFrame content = new UpdateProductFrame();
		updateProductFrame.setContentPane(content);
		updateProductFrame.setLocation(500, 300);
		updateProductFrame.setSize(new Dimension(500,150));
		updateProductFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		updateProductFrame.setVisible(true);
		copyFrame = updateProductFrame;
	}
	
	JLabel productName;
	JTextField productNameField;
	JButton updateButton;
	
	public UpdateProductFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.GREEN);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.GREEN);
		centerPanel.setLayout(new GridLayout(1,2));
		this.add(centerPanel,BorderLayout.CENTER);
		
		productName = new JLabel("Product's Name");
		centerPanel.add(productName);
		
		productNameField = new JTextField();
		productNameField.setPreferredSize(new Dimension(50,50));
		centerPanel.add(productNameField);
		
		updateButton = new JButton("Update Product by Name");
		updateButton.addActionListener(this);
		this.add(updateButton,BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String s = e.getActionCommand();
		if(s.equals("Update Product by Name"))
		{
			int id = ProductDAO.getProductId(productNameField.getText());
			try
			{
				if(id == 0)
				{
					throw new SQLException("Acest produs nu se afla in baza de date!");
				}
				else
				{
					UpdateFrame.triggerWindow(id);
					copyFrame.dispose();
				}
			}
			catch(SQLException ex)
			{
				JFrame errorFrame = new JFrame("Eroare");
				JPanel errorPanel = new JPanel();
				JLabel errorLabel = new JLabel(ex.getMessage());
				errorPanel.add(errorLabel);
				errorFrame.setContentPane(errorPanel);
				errorFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				errorFrame.setSize(200,115);
				errorFrame.setLocation(400,400);
				errorFrame.setVisible(true);
				errorFrame.setEnabled(true);
			}
		}
	}

}
