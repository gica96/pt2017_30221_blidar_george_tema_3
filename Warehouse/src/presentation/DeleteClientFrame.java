package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataAccessObject.CustomerDAO;

public class DeleteClientFrame extends JPanel implements ActionListener 
{
	private static final long serialVersionUID = 1L;
	static JFrame copyFrame;
	public static void triggerWindow()
	{
		JFrame deleteClientFrame = new JFrame("Delete Client");
		DeleteClientFrame content = new DeleteClientFrame();
		deleteClientFrame.setContentPane(content);
		deleteClientFrame.setLocation(500, 300);
		deleteClientFrame.setSize(new Dimension(500,150));
		deleteClientFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		deleteClientFrame.setVisible(true);
		copyFrame = deleteClientFrame;
	}
	
	JLabel clientName;
	JTextField clientNameField;
	JButton deleteButton;
	
	public DeleteClientFrame()
	{
		this.setLayout(new BorderLayout());
		this.setBackground(Color.cyan);
		
		JPanel centerPanel = new JPanel();
		centerPanel.setBackground(Color.cyan);
		centerPanel.setLayout(new GridLayout(1,2));
		this.add(centerPanel,BorderLayout.CENTER);
		
		clientName = new JLabel("Customer's Name");
		centerPanel.add(clientName);
		
		clientNameField = new JTextField();
		clientNameField.setPreferredSize(new Dimension(50,50));
		centerPanel.add(clientNameField);
		
		deleteButton = new JButton("Delete Customer by Name");
		deleteButton.addActionListener(this);
		this.add(deleteButton,BorderLayout.SOUTH);
	}

	@Override
	public void actionPerformed(ActionEvent evt) 
	{
		String s = evt.getActionCommand();
		if(s.equals("Delete Customer by Name"))
		{
			int id = CustomerDAO.getCustomerId(clientNameField.getText());
			CustomerDAO.deleteCustomer(id);
		}
	}

}
