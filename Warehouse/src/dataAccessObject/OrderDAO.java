package dataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dataAccess.ConnectionFactory;

public class OrderDAO {
		
		public static void insertOrder(int customerId, int productId)
		{
			String insertStringStatement1 = "insert into OrderTable (CustomerID) values(?)";
			String insertStringStatement2 = "insert into OrderProduct (OrderID,ProductID) values(?,?)";
			String selectStringStatement = "select OrderTable.OrderID from OrderTable where OrderTable.CustomerId ='"+customerId+"'";
			
			Connection connection = ConnectionFactory.getConnection();
			PreparedStatement insertStatement1 = null;
			PreparedStatement insertStatement2 = null;
			PreparedStatement selectStatement = null;
			ResultSet rs = null;
			int orderId = 0;
			
			try
			{
				insertStatement1 = connection.prepareStatement(insertStringStatement1);
				insertStatement1.setInt(1, customerId);
				insertStatement1.executeUpdate();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			
			try
			{
				selectStatement = connection.prepareStatement(selectStringStatement);
				rs = selectStatement.executeQuery();
				while(rs.next())
				{
					orderId = rs.getInt("OrderID");
				}
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			
			try
			{
				insertStatement2 = connection.prepareStatement(insertStringStatement2);
				insertStatement2.setInt(1, orderId);
				insertStatement2.setInt(2, productId);
				insertStatement2.executeUpdate();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
			
			finally
			{
				ConnectionFactory.closeResultSet(rs);
				ConnectionFactory.closeStatement(selectStatement);
				ConnectionFactory.closeStatement(insertStatement1);
				ConnectionFactory.closeStatement(insertStatement2);


			}
		}
}
