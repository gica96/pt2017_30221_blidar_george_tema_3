package dataAccessObject;

import java.lang.reflect.Field;
import java.util.Vector;

public class Table 
{
	
	public static Vector<Object> retrieveProperties(Object object) 
	{
		
		Vector<Object> row = new Vector<Object>();
		for (Field field : object.getClass().getDeclaredFields()) 
		{
			field.setAccessible(true); // set modifier to public
			Object value;
			try 
			{
				value = field.get(object);
				row.add(value);
			} 
			catch (IllegalArgumentException e) 
			{
				e.printStackTrace();
			} 
			catch (IllegalAccessException e) 
			{
				e.printStackTrace();
			}
		}
		return row;	
	}
}
