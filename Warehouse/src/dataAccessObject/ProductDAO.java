package dataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import dataAccess.ConnectionFactory;
import model.Product;
import model.Stock;

public class ProductDAO 
{
	public static int insertProduct(Product p)
	{
		int checkValue = 1;
		String insertStatementString = "INSERT INTO Product (ProductID,theName,Description,Price) VALUES(?,?,?,?)"; 
		String insertStockStatementString = "INSERT INTO Stock(ProductID,Quantity) VALUES(?,?)";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		PreparedStatement insertStockStatement = null;
		try
		{
			insertStatement = connection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, p.getProductId());
			insertStatement.setString(2, p.getName());
			insertStatement.setString(3, p.getDescription());
			insertStatement.setDouble(4, p.getPrice());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
		
			insertStockStatement = connection.prepareStatement(insertStockStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStockStatement.setInt(1, p.getProductId());
			insertStockStatement.setInt(2, 15);
			insertStockStatement.executeUpdate();
			
			if(!rs.next())
			{
				checkValue = 0;
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(insertStockStatement);
			ConnectionFactory.closeStatement(insertStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return checkValue;
	}
	
	public static int getProductId(String name)
	{
		int id = 0;
		String findId = "SELECT Product.ProductID from Product WHERE Product.theName = '" + name + "'";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try
		{
			findStatement = connection.prepareStatement(findId);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
				id = rs.getInt("ProductID");
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(findStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return id;
	}
	
	public static void deleteProduct(int id)
	{
		String firstDelete = "delete from S using Stock as S where S.productID ='"+id+"'";
		String secondDelete = "delete from P using Product as P where P.productID ='"+id+"'";
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement1 = null;
		PreparedStatement deleteStatement2 = null;
		
		try
		{
			deleteStatement1 = connection.prepareStatement(firstDelete);
			deleteStatement1.executeUpdate();
			
			deleteStatement2 = connection.prepareStatement(secondDelete);
			deleteStatement2.executeUpdate();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(deleteStatement2);
			ConnectionFactory.closeStatement(deleteStatement1);
			ConnectionFactory.closeConnection(connection);
		}
	}
	
	public static void updateProduct(int id, int quantity, double price)
	{
		String firstUpdate = "update Product set Product.Price ='"+price+"'"+" where Product.ProductID ='"+id+"'";
		String secondUpdate = "update Stock set Stock.quantity ='"+quantity+"'"+" where Stock.ProductID ='"+id+"'";
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement1 = null;
		PreparedStatement updateStatement2 = null;
		
		try
		{
			updateStatement1 = connection.prepareStatement(firstUpdate);
			updateStatement1.executeUpdate();
			
			updateStatement2 = connection.prepareStatement(secondUpdate);
			updateStatement2.executeUpdate();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(updateStatement2);
			ConnectionFactory.closeStatement(updateStatement1);
			ConnectionFactory.closeConnection(connection);
		}
	}
	
	public static void updateStock(int id, int quantity)
	{
		String	firstUpdate = "update Stock set Stock.quantity ='"+quantity+"'"+" where Stock.ProductID ='"+id+"'";
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement1 = null;
		
		try
		{
			updateStatement1 = connection.prepareStatement(firstUpdate);
			updateStatement1.executeUpdate();
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(updateStatement1);
			ConnectionFactory.closeConnection(connection);
		}
	}
	
	public static double getPrice(int id)
	{
		String selectStringStatement = "select Product.Price from Product where Product.ProductID ='"+id+"'";
		double price = 0;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		
		try
		{
			selectStatement  = connection.prepareStatement(selectStringStatement);
			rs = selectStatement.executeQuery();
			while(rs.next())
			{
				price = rs.getDouble("Price");
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(selectStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return price;
	}
	
	public static ArrayList<Product> getAllProducts()
	{
		ArrayList<Product> products = new ArrayList<Product>();
		String selectString = "select * from Product";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		try
		{
			selectStatement = connection.prepareStatement(selectString);
			rs = selectStatement.executeQuery();
			while(rs.next())
			{
				int id = rs.getInt("ProductID");
				String name = rs.getString("theName");
				String description = rs.getString("Description");
				double price = rs.getDouble("Price");
				Product p = new Product(id,name,description,price);
				products.add(p);
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(selectStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return products;
	}
	
	public static ArrayList<Stock> getStock()
	{
		ArrayList<Stock> stock = new ArrayList<Stock>();
		String selectString = "select * from Stock";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		try
		{
			selectStatement = connection.prepareStatement(selectString);
			rs = selectStatement.executeQuery();
			while(rs.next())
			{
				int id = rs.getInt("ProductID");
				int quantity = rs.getInt("Quantity");
				Stock s = new Stock(id,quantity);
				stock.add(s);
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(selectStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return stock;
	}
}
