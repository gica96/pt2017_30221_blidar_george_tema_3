package dataAccessObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import dataAccess.ConnectionFactory;
import model.Customer;

public class CustomerDAO {
	
	public static int[] identifyCustomer(String name, String pass)
	{
		int result[] = new int[2];
		result[0] = -1;
		result[1] = -1;
		String findNameAndPass = "SELECT Customer.theName, Customer.thePassword, Customer.Admin FROM Customer";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try
		{
			findStatement = connection.prepareStatement(findNameAndPass);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
				String theName = rs.getString("theName");
				String thePass = rs.getString("thePassword");
				if(theName.equals(name))
				{
					if(thePass.equals(pass))
					{
						int admin = rs.getInt("Admin");
						result[0] = 1;
						result[1] = admin;
						break;
					}
					else
					{
						result[0] = 0;
						result[1] = 0;
					}
				}
				else
				{
					result[0] = -1;
					result[1] = 0;
				}
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(findStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return result;
	}
	
	public static String getEmailAdress(int id)
	{
		String findString = "select Customer.Email from Customer where Customer.CustomerID ='"+id+"'";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		String email = "";
		
		try
		{
			findStatement = connection.prepareStatement(findString);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
				email = rs.getString("Email");
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(findStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return email;
	}
	
	public static int getCustomerId(String name)
	{
		int id = 0;
		String findId = "SELECT Customer.CustomerID from Customer WHERE Customer.theName = '" + name + "'";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try
		{
			findStatement = connection.prepareStatement(findId);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
				id = rs.getInt("CustomerID");
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(findStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return id;
	}
	
	public static int insertCustomer(Customer c)
	{
		int checkValue = 1;
		String insertStatementString = "INSERT INTO Customer (CustomerID,theName,thePassword,Phone_Number,Adress,Email,Admin) VALUES(?,?,?,?,?,?,?)"; 
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		try
		{
			insertStatement = connection.prepareStatement(insertStatementString,Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, c.getCustomerId());
			insertStatement.setString(2, c.getName());
			insertStatement.setString(3, c.getPassword());
			insertStatement.setString(4, c.getNumber());
			insertStatement.setString(5,c.getAdress());
			insertStatement.setString(6, c.getEmail());
			insertStatement.setInt(7, c.getAdmin());
			insertStatement.executeUpdate();
			
			ResultSet rs = insertStatement.getGeneratedKeys();
			if(!rs.next())
			{
				checkValue = 0;
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(insertStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return checkValue;
	}
	
	public static void deleteCustomer(int id)
	{
		String firstDelete = "delete from OP using OrderProduct as OP, OrderTable as OT, Customer as C where C.CustomerID ='"+id+"'"+" and C.CustomerID = OT.CustomerID and OT.OrderID = OP.OrderID";
		String secondDelete = "delete from OT using OrderTable as OT, Customer as C where C.CustomerID ='"+id+"'"+" and C.CustomerID = OT.CustomerID";
		String thirdDelete = "delete from C using Customer as C where C.CustomerID ='"+id+"'";
		
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement1 = null;
		PreparedStatement deleteStatement2 = null;
		PreparedStatement deleteStatement3 = null;
		try
		{
			deleteStatement1 = connection.prepareStatement(firstDelete);
			deleteStatement1.executeUpdate();
			
			deleteStatement2 = connection.prepareStatement(secondDelete);
			deleteStatement2.executeUpdate();
			
			deleteStatement3 = connection.prepareStatement(thirdDelete);
			deleteStatement3.executeUpdate();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeStatement(deleteStatement3);
			ConnectionFactory.closeStatement(deleteStatement2);
			ConnectionFactory.closeStatement(deleteStatement1);
			ConnectionFactory.closeConnection(connection);
		}
	}
	
	public static ArrayList<Customer> getAllCustomers()
	{
		ArrayList<Customer> customers = new ArrayList<Customer>();
		String selectString = "select * from Customer";
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		try
		{
			selectStatement = connection.prepareStatement(selectString);
			rs = selectStatement.executeQuery();
			while(rs.next())
			{
				int id = rs.getInt("CustomerID");
				String name = rs.getString("theName");
				String pass = rs.getString("thePassword");
				String phone = rs.getString("Phone_Number");
				String adress = rs.getString("Adress");
				String email = rs.getString("Email");
				int admin = rs.getInt("Admin");
				Customer c = new Customer(id,name,pass,phone,adress,email,admin);
				customers.add(c);
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			ConnectionFactory.closeResultSet(rs);
			ConnectionFactory.closeStatement(selectStatement);
			ConnectionFactory.closeConnection(connection);
		}
		return customers;
	}
}