package productValidator;

public class ProductValidator 
{
	public boolean validateName(String name)
	{
		if(name == null) // nume null
		{
			return false;
		}
		else if(name.length() > 50) // lungimea numelui > 50
		{
			return false;
		}
		else if(name.charAt(0) < 65 || name.charAt(0) > 90) // nu incepe cu litera mare
		{
			return false;
		}
		return true;
	}
	
	public boolean validateId(int id)
	{
		if( id <= 0)
		{
			return false;
		}
		return true;
	}
	
	public boolean validatePrice(double price)
	{
		if(price <=0 )
		{
			return false;
		}
		return true;
	}
	
	public boolean validateQuantity(int value)
	{
		if(value <= 0)
		{
			return false;
		}
		return true;
	}
}
