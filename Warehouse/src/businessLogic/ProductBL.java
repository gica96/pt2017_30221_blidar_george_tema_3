package businessLogic;

import model.Product;
import productValidator.ProductValidator;

public class ProductBL {
	
	public boolean validateProduct(Product product)
	{
		ProductValidator validator = new ProductValidator();
		boolean isGood;
		
		isGood = validator.validateId(product.getProductId());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validateName(product.getName());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validatePrice(product.getPrice());
		if(isGood == false)
		{
			return false;
		}
		return true;
	}
}
