package businessLogic;

import customerValidator.CustomerValidator;
import model.Customer;

public class CustomerBL {
	
	public boolean validateCustomer(Customer customer)
	{
		CustomerValidator validator = new CustomerValidator();
		boolean isGood;
		
		isGood = validator.validateId(customer.getCustomerId());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validateName(customer.getName());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validatePassword(customer.getPassword());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validateAdress(customer.getAdress());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validateEmail(customer.getEmail());
		if(isGood == false)
		{
			return false;
		}
		
		isGood = validator.validatePhoneNumber(customer.getNumber());
		if(isGood == false)
		{
			return false;
		}
		return true;
	}
}
