package dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionFactory {
	
	private static final String URL = "jdbc:mysql://localhost:3306/Warehouse";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	private static ConnectionFactory singleConnection = new ConnectionFactory(); // singleton pattern
	
	private ConnectionFactory()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch(ClassNotFoundException ex)
		{
			ex.printStackTrace();
		}
	}
	
	private Connection newConnection()
	{
		Connection conn = null;
		try
		{
			conn = DriverManager.getConnection(URL,USER,PASS);
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		return conn;
	}
	
	public static Connection getConnection()
	{
		return singleConnection.newConnection();
	}

	public static void closeConnection(Connection conn)
	{
		if(conn != null)
		{
			try
			{
				conn.close();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public static void closeStatement(Statement statement)
	{
		if(statement != null)
		{
			try
			{
				statement.close();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	public static void closeResultSet(ResultSet rs)
	{
		if(rs != null)
		{
			try
			{
				rs.close();
			}
			catch(SQLException ex)
			{
				ex.printStackTrace();
			}
		}
	}
}
