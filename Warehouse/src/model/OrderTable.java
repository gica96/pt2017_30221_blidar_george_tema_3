package model;

public class OrderTable {
	
	private int orderId;
	private int customerId;
	
	public OrderTable(int customerId)
	{
		this.customerId = customerId;
	}
	
	public OrderTable(int orderId, int customerId)
	{
		this.orderId = orderId;
		this.customerId = customerId;
	}
	
	public int getOrderId()
	{
		return this.orderId;
	}
	
	public void setOrderId(int value)
	{
		this.orderId = value;
	}
	
	public int getCustomerId()
	{
		return this.customerId;
	}
	
	public void setCustomerId(int value)
	{
		this.customerId = value;
	}

}
