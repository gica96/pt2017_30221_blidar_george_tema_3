package model;

public class OrderProduct {
	
	private int id;
	private int orderId;
	private int productId;
	
	public OrderProduct(int orderId, int productId)
	{
		this.orderId = orderId;
		this.productId = productId;
	}
	
	public OrderProduct(int id, int orderId, int productId)
	{
		this.id = id;
		this.orderId = orderId;
		this.productId = productId;
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int value)
	{
		this.id = value;
	}
	
	public int getOrderId()
	{
		return this.orderId;
	}
	
	public void setOrderId(int value)
	{
		this.orderId = value;
	}
	
	public int getProductId()
	{
		return this.productId;
	}
	
	public void setProductId(int value)
	{
		this.productId = value;
	}

}
