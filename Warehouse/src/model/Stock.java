package model;

public class Stock {
	
	private int productId;
	private int quantity;
	
	public Stock(int productId, int quantity)
	{
		this.productId = productId;
		this.quantity = quantity;
	}
	
	public int getProductId()
	{
		return this.productId;
	}
	
	public void setProductId(int value)
	{
		this.productId = value;
	}
	
	public int getQuantity()
	{
		return this.quantity;
	}
	
	public void setQuantity(int value)
	{
		this.quantity = value;
	}

}
