package model;

public class Product {
	
	private int productId;
	private String name;
	private String description;
	private double price;
	
	public Product(String name, String description, double price)
	{
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	public Product(int id, String name, String description, double price)
	{
		this.productId = id;
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	public int getProductId()
	{
		return this.productId;
	}
	
	public void setProductId(int value)
	{
		this.productId = value;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String value)
	{
		this.name = value;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public void setDescription(String value)
	{
		this.description = value;
	}
	
	public double getPrice()
	{
		return this.price;
	}
	
	public void setPrice(double value)
	{
		this.price = value;
	}

}
