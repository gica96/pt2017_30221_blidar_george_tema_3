package model;

public class Customer {
	
	private int customerId;
	private String name;
	private String password;
	private String phoneNumber;
	private String adress;
	private String email;
	private int admin;
	
	public Customer(String name, String password, String number, String adress, String email, int admin)
	{
		this.name = name;
		this.password = password;
		this.phoneNumber = number;
		this.adress = adress;
		this.email = email;
		this.admin = admin;
	}
	
	public Customer(int id, String name, String password, String number, String adress, String email, int admin)
	{
		this.customerId = id;
		this.name = name;
		this.password = password;
		this.phoneNumber = number;
		this.adress = adress;
		this.email = email;
		this.admin = admin;
	}
	
	public int getCustomerId()
	{
		return this.customerId;
	}
	
	public void setCustomerId(int value)
	{
		this.customerId = value;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getNumber()
	{
		return this.phoneNumber;
	}
	
	public void setNumber(String value)
	{
		this.phoneNumber = value;
	}
	
	public String getAdress()
	{
		return this.adress;
	}
	
	public void setAdress(String adress)
	{
		this.adress = adress;
	}
	
	public String getEmail()
	{
		return this.email;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	public String getPassword()
	{
		return this.password;
	}
	
	public void setPassword(String value)
	{
		this.password = value;
	}
	
	public int getAdmin()
	{
		return this.admin;
	}
	
	public void setAdmin(int value)
	{
		this.admin = value;
	}
}
